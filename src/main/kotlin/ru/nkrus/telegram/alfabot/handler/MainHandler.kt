package ru.nkrus.telegram.alfabot.handler

import mu.KotlinLogging
import org.apache.shiro.session.Session
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText
import org.telegram.telegrambots.meta.api.objects.CallbackQuery
import org.telegram.telegrambots.meta.api.objects.Message
import ru.nkrus.telegram.alfabot.*
import ru.nkrus.telegram.alfabot.bot.Bot
import ru.nkrus.telegram.alfabot.model.UserRole
import ru.nkrus.telegram.alfabot.model.message.DailyJBAWReport
import ru.nkrus.telegram.alfabot.repository.PersonRepository
import ru.nkrus.telegram.alfabot.service.*
import ru.nkrus.telegram.alfabot.util.currentDate

@Component
class MainHandler(
        val personRepository: PersonRepository,
        val dailyBAWService: DailyBAWService,
        val dailyPlanService: DailyPlanService,
        val dailyJbawService: DailyJbawService,
        val reportGeneratorService: DailyReportGeneratorService,
        val matchingService: MatchingService,
        val notificationService: NotificationService
) {
    val log = KotlinLogging.logger { }

    val commandsByRoles = mapOf(
            UserRole.HEAD_OF_DEPARTMENT
                    to listOf(Commands.COMMAND_PLAN, Commands.COMMAND_STATUS_DAY, Commands.COMMAND_GENERATE_REPORT, Commands.COMMAND_PLAN_NEW,
                    Commands.COMMAND_REPORTS_HEAD_OF_GROUPS, Commands.COMMAND_ADD_BAW_REPORT_NEW_INSTEAD),
            UserRole.HEAD_OF_GROUP
                    to listOf(Commands.COMMAND_ADD_BAW_REPORT, Commands.COMMAND_ADD_BAW_REPORT_NEW),
            UserRole.COORDINATOR
                    to listOf(Commands.COMMAND_PLAN, Commands.COMMAND_ADD_JBAW_REPORT, Commands.COMMAND_GENERATE_REPORT,
                    Commands.COMMAND_STATUS_DAY, Commands.COMMAND_REPORTS_HEAD_OF_GROUPS,
                    Commands.COMMAND_PLAN_NEW, Commands.COMMAND_ADD_JBAW_REPORT_NEW, Commands.COMMAND_ADD_BAW_REPORT_NEW_INSTEAD)
    )

    fun handle(callbackQuery: CallbackQuery, session: Session, bot: Bot) {
        val person = personRepository.findPersonByChatId(callbackQuery.message.chatId)
        if (person?.role == null) {
            return
        }
        when {
            callbackQuery.data == States.BAW_NEW.name -> askBawReportCommand(callbackQuery, session, bot)
            callbackQuery.data == States.PLAN_NEW.name -> askPlan(callbackQuery, session, bot)
            callbackQuery.data == States.JBAW_NEW.name -> askJbawReportCommand(callbackQuery, session, bot)
            callbackQuery.data == States.SHARE_REPORT.name -> shareReportToGroup(callbackQuery, bot)
            session.getAttribute(Commands.SESSION_LAST_COMMAND) == Commands.COMMAND_REPORTS_HEAD_OF_GROUPS ->
                onChooseHeadOfGroup(callbackQuery, session, bot)
            session.getAttribute(Commands.SESSION_LAST_COMMAND) == Commands.COMMAND_CHOOSE_HEAD_OF_GROUP ->
                onAddBawReportInstead(callbackQuery, session, bot)
        }
    }

    fun handle(message: Message, session: Session, bot: Bot) {
        val role = personRepository.findPersonByChatId(message.chatId)?.role
        val text = message.text
        val lastCommand = session.getAttribute(Commands.SESSION_LAST_COMMAND)
        log.info { "MainHandler lastCommand: ${session.getAttribute(Commands.SESSION_LAST_COMMAND)} message: ${message.text}" }
        if (role == null || (commandsByRoles.getOrDefault(role, listOf()).none { it.text == text || it === lastCommand })) {
            log.error { "Unknown command for this role" }
            return
        }
        when {
            text?.startsWith(Commands.COMMAND_ADD_BAW_REPORT.text) == true -> onAddBawReportCommand(message, session, bot)
            text?.startsWith(Commands.COMMAND_PLAN.text) == true -> onPlanCommand(message, session, bot)
            text?.startsWith(Commands.COMMAND_STATUS_DAY.text) == true -> onStatusDayCommand(message, bot)
            text?.startsWith(Commands.COMMAND_GENERATE_REPORT.text) == true -> onGenerateReportCommand(message, bot)
            text?.startsWith(Commands.COMMAND_ADD_JBAW_REPORT.text) == true -> onAddJbawReportCommand(message, session, bot)
            text?.startsWith(Commands.COMMAND_REPORTS_HEAD_OF_GROUPS.text) == true -> onReportsHeadOfGroups(message, session, bot)

            Commands.COMMAND_ADD_BAW_REPORT_NEW == session.getAttribute(Commands.SESSION_LAST_COMMAND) -> addBawReport(message, session, bot)
            Commands.COMMAND_PLAN_NEW == session.getAttribute(Commands.SESSION_LAST_COMMAND) -> addPlan(message, session, bot)
            Commands.COMMAND_ADD_JBAW_REPORT_NEW == session.getAttribute(Commands.SESSION_LAST_COMMAND) -> addJbawReport(message, session, bot)
            Commands.COMMAND_ADD_BAW_REPORT_NEW_INSTEAD == session.getAttribute(Commands.SESSION_LAST_COMMAND) -> addBawReportInstead(message, session, bot)
        }
    }

    private fun onAddBawReportCommand(message: Message, session: Session, bot: Bot) {
        log.info { "Handling onAddBawReportCommand" }

        session.setAttribute(Commands.SESSION_LAST_COMMAND, Commands.COMMAND_ADD_BAW_REPORT)
        val bawReport = dailyBAWService.findByChatIdAndDate(message.chatId, currentDate())

        bot.execute(bawReport(bawReport).setChatId(message.chatId))
    }

    private fun askBawReportCommand(callbackQuery: CallbackQuery, session: Session?, bot: Bot) {
        log.info { "Handling askBawReportCommand" }

        session?.setAttribute(Commands.SESSION_LAST_COMMAND, Commands.COMMAND_ADD_BAW_REPORT_NEW)

        bot.execute(EditMessageText()
                .setChatId(callbackQuery.message.chatId)
                .setMessageId(callbackQuery.message.messageId)
                .setText("Окей, пришли мне ежедневный BAW отчет в формате, как ты присылаешь в группу своим коллегам в WhatsApp.$THINKING"))
    }

    private fun addBawReport(message: Message, session: Session, bot: Bot) {
        log.info { "Handling addBawReport" }

        val person = personRepository.findPersonByChatId(message.chatId)!!
        dailyBAWService.deleteByChatIdAndCreationDate(message.chatId, currentDate())
        val newDailyBaw = matchingService.matchDailyBaw(message)
        newDailyBaw.department = person.department

        val dailyBaw = dailyBAWService.save(newDailyBaw)

        session.removeAttribute(Commands.SESSION_LAST_COMMAND)
        bot.execute(SendMessage()
                .setChatId(message.chatId)
                .setText("$DONE BAW отчет за сегодня успешно добавлен!\n$dailyBaw"))
        notificationService.sendNotification(Notifications.NEW_DAILY_BAW_REPORT, person, bot, message = dailyBaw.toString())
    }

    private fun onPlanCommand(message: Message, session: Session, bot: Bot) {
        log.info { "Handling onPlanCommand" }

        session.setAttribute(Commands.SESSION_LAST_COMMAND, Commands.COMMAND_PLAN)
        val person = personRepository.findPersonByChatId(message.chatId)
        val dailyPlan = dailyPlanService.findByDepartmentAndCreationDate(person?.department!!, currentDate())

        bot.execute(dailyPlan(dailyPlan).setChatId(message.chatId))
    }

    private fun askPlan(callbackQuery: CallbackQuery, session: Session?, bot: Bot) {
        log.info { "Handling askPlan" }

        session?.setAttribute(Commands.SESSION_LAST_COMMAND, Commands.COMMAND_PLAN_NEW)

        bot.execute(EditMessageText()
                .setChatId(callbackQuery.message.chatId)
                .setMessageId(callbackQuery.message.messageId)
                .setText("Окей, пришли мне план на сегодня в следующем формате:\n" +
                        "PIL 20\n" +
                        "CC 21\n"))
    }

    private fun addPlan(message: Message, session: Session, bot: Bot) {
        log.info { "Handling addPlan" }

        val person = personRepository.findPersonByChatId(message.chatId)
        dailyPlanService.deleteByDepartmentAndCreationDate(person?.department!!, currentDate())

        val newDailyPlan = matchingService.matchDailyPlan(message)
        newDailyPlan.person = person
        newDailyPlan.department = person.department
        val dailyPlan = dailyPlanService.save(newDailyPlan)

        session.removeAttribute(Commands.SESSION_LAST_COMMAND)
        bot.execute(SendMessage()
                .setChatId(message.chatId)
                .setText("$DONE План на сегодня успешно добавлен!\n$dailyPlan"))
        notificationService.sendNotification(Notifications.NEW_DAILY_PLAN, person, bot, message = dailyPlan.toString())
    }

    private fun onStatusDayCommand(message: Message, bot: Bot) {
        log.info { "Handling onStatusDayCommand" }

        val person = personRepository.findPersonByChatId(message.chatId)
        val dailyPlan = dailyPlanService.findByDepartmentAndCreationDate(person?.department!!, currentDate())
        val dailyJbaw = dailyJbawService.findByDepartmentAndCreationDate(person.department!!, currentDate())
        val bawReportStatusByUser = person.department?.persons
                ?.filter { UserRole.HEAD_OF_GROUP == it.role }
                ?.map {
                    it.name to (dailyBAWService.findByChatIdAndDate(it.chatId!!, currentDate())?.let { true }
                            ?: false)
                }
                .orEmpty()

        bot.execute(statusDay(dailyPlan, dailyJbaw, bawReportStatusByUser).setChatId(message.chatId))
    }


    private fun onGenerateReportCommand(message: Message, bot: Bot) {
        log.info { "Handling onGenerateReportCommand" }

        val person = personRepository.findPersonByChatId(message.chatId)
        val report = reportGeneratorService.generateReport(person?.department!!, currentDate())
        bot.execute(report
                .setChatId(message.chatId))
    }

    private fun onAddJbawReportCommand(message: Message, session: Session, bot: Bot) {
        log.info { "Handling onAddJbawReportCommand" }

        session.setAttribute(Commands.SESSION_LAST_COMMAND, Commands.COMMAND_ADD_JBAW_REPORT)
        val jBawReport = dailyJbawService.findByChatIdAndDate(message.chatId, currentDate())

        bot.execute(jBawReport(jBawReport).setChatId(message.chatId))
    }

    private fun askJbawReportCommand(callbackQuery: CallbackQuery, session: Session?, bot: Bot) {
        log.info { "Handling askJbawReportCommand" }

        session?.setAttribute(Commands.SESSION_LAST_COMMAND, Commands.COMMAND_ADD_JBAW_REPORT_NEW)

        bot.execute(EditMessageText()
                .setChatId(callbackQuery.message.chatId)
                .setMessageId(callbackQuery.message.messageId)
                .setText("Окей, пришли мне ежедневный JBAW отчет как обычно.$HEAD_DOWN"))
    }

    private fun addJbawReport(message: Message, session: Session, bot: Bot) {
        log.info { "Handling addJbawReport" }

        val person = personRepository.findPersonByChatId(message.chatId)!!
        dailyJbawService.deleteByChatIdAndCreationDate(message.chatId, currentDate())
        val dailyJBAWReport = matchingService.matchJbawReport(message)
        dailyJBAWReport.department = person.department
        dailyJbawService.save(dailyJBAWReport)

        session.removeAttribute(Commands.SESSION_LAST_COMMAND)
        bot.execute(SendMessage()
                .setChatId(message.chatId)
                .setText("$DONE JBAW отчет за сегодня успешно добавлен!\n$dailyJBAWReport"))
        notificationService.sendNotification(Notifications.NEW_DAILY_JBAW_REPORT, person, bot)
    }

    private fun onReportsHeadOfGroups(message: Message, session: Session, bot: Bot) {
        log.info { "Handling onReportsHeadOfGroups" }

        session.setAttribute(Commands.SESSION_LAST_COMMAND, Commands.COMMAND_REPORTS_HEAD_OF_GROUPS)
        val person = personRepository.findPersonByChatId(message.chatId)
        val dailyBawStatusByPerson = person?.department?.persons
                ?.filter { UserRole.HEAD_OF_GROUP == it.role }
                ?.map {
                    it to (dailyBAWService.findByChatIdAndDate(it.chatId!!, currentDate())?.let { true }
                            ?: false)
                }
                .orEmpty()
        bot.execute(headOfGroupsBawReports(dailyBawStatusByPerson).setChatId(message.chatId))
    }

    private fun onChooseHeadOfGroup(callbackQuery: CallbackQuery, session: Session, bot: Bot) {
        log.info { "Handling onChooseHeadOfGroup" }

        val chatId = callbackQuery.data
        val person = personRepository.findPersonByChatId(chatId.toLong())
        val bawReport = dailyBAWService.findByChatIdAndDate(chatId.toLong(), currentDate())

        session.setAttribute(Commands.SESSION_LAST_COMMAND, Commands.COMMAND_CHOOSE_HEAD_OF_GROUP)
        bot.execute(bawReportInstead(bawReport, chatId, person)
                .setChatId(callbackQuery.message.chatId)
                .setMessageId(callbackQuery.message.messageId))
    }

    private fun onAddBawReportInstead(callbackQuery: CallbackQuery, session: Session, bot: Bot) {
        log.info { "Handling onAddBawReportInstead" }

        val chatId = callbackQuery.data
        val person = personRepository.findPersonByChatId(chatId.toLong())
        session.setAttribute(Commands.SESSION_LAST_COMMAND, Commands.COMMAND_ADD_BAW_REPORT_NEW_INSTEAD)
        session.setAttribute(Commands.SESSION_INSTEAD_CHAT_ID, chatId)

        bot.execute(EditMessageText()
                .setChatId(callbackQuery.message.chatId)
                .setMessageId(callbackQuery.message.messageId)
                .setText("Окей, пришли мне ежедневный BAW отчет для - ${person?.name}$THINKING"))
    }

    private fun addBawReportInstead(message: Message, session: Session, bot: Bot) {
        log.info { "Handling addBawReportInstead" }
        val chatId = (session.getAttribute(Commands.SESSION_INSTEAD_CHAT_ID) as String).toLong()

        val editor = personRepository.findPersonByChatId(message.chatId)!!
        val insteadPerson = personRepository.findPersonByChatId(chatId)!!
        dailyBAWService.deleteByChatIdAndCreationDate(chatId, currentDate())
        val newDailyBaw = matchingService.matchDailyBaw(message)

        newDailyBaw.department = insteadPerson.department
        newDailyBaw.chatId = chatId
        val dailyBaw = dailyBAWService.save(newDailyBaw)

        session.removeAttribute(Commands.SESSION_LAST_COMMAND)
        session.removeAttribute(Commands.SESSION_INSTEAD_CHAT_ID)
        bot.execute(SendMessage()
                .setChatId(message.chatId)
                .setText("$DONE BAW отчет пользователя - ${insteadPerson.name.orEmpty()} успешно добавлен!\n$dailyBaw"))
        notificationService.sendNotification(Notifications.NEW_DAILY_BAW_REPORT_INSTEAD, editor, bot,
                message = dailyBaw.toString(), instead = insteadPerson)
    }


    private fun shareReportToGroup(callbackQuery: CallbackQuery, bot: Bot) {
        log.info { "Handling shareReportToGroup"  }
        val person = personRepository.findPersonByChatId(callbackQuery.message.chatId)
        bot.execute(EditMessageText()
                .setChatId(callbackQuery.message.chatId)
                .setMessageId(callbackQuery.message.messageId)
                .setText("${callbackQuery.message.text}\nОтчет отправлен всем участникам группы $FINGER_UP"))
        notificationService.sendNotification(Notifications.REPORT_CREATED, person!!, bot, callbackQuery.message.text)
    }
}