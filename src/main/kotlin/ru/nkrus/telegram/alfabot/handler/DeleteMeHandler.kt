package ru.nkrus.telegram.alfabot.handler

import mu.KotlinLogging
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Message
import ru.nkrus.telegram.alfabot.Commands
import ru.nkrus.telegram.alfabot.HI_HAND
import ru.nkrus.telegram.alfabot.THINKING
import ru.nkrus.telegram.alfabot.bot.Bot
import ru.nkrus.telegram.alfabot.repository.DailyBawDataRepository
import ru.nkrus.telegram.alfabot.repository.DailyJbawRepository
import ru.nkrus.telegram.alfabot.repository.DailyPlanRepository
import ru.nkrus.telegram.alfabot.repository.PersonRepository

@Component
class DeleteMeHandler(
        val personRepository: PersonRepository,
        val dailyBawDataRepository: DailyBawDataRepository,
        val dailyJbawRepository: DailyJbawRepository,
        val dailyPlanRepository: DailyPlanRepository
) {
    val log = KotlinLogging.logger { }

    @Transactional
    fun handle(message: Message, bot: Bot) {
        log.info { "Trying to delete chatId: ${message.chatId}" }
        val personId = personRepository.findPersonByChatId(message.chatId)?.id
        if (personId == null) {
            bot.execute(SendMessage()
                    .setChatId(message.chatId)
                    .setText("Я тебя еще не регистрировал. Удалять пока нечего.$THINKING"))
        } else {
            log.info { "Deleting person with id: $personId chatId: ${message.chatId}" }
            dailyBawDataRepository.deleteAllByChatId(message.chatId)
            dailyJbawRepository.deleteAllByChatId(message.chatId)
            dailyPlanRepository.deleteAllByChatId(message.chatId)
            personRepository.deleteById(personId)
            bot.execute(SendMessage()
                    .setChatId(message.chatId)
                    .setReplyMarkup(null)
                    .setText("Я успешно удалил твой аккаунт из своей базы. Bye Bye$HI_HAND\n" +
                            "Чтобы зарегестрироваться снова отправь команду ${Commands.START_COMMAND.text}"))
        }


    }
}