package ru.nkrus.telegram.alfabot.handler

import mu.KotlinLogging
import org.apache.shiro.session.Session
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText
import org.telegram.telegrambots.meta.api.objects.CallbackQuery
import org.telegram.telegrambots.meta.api.objects.Message
import ru.nkrus.telegram.alfabot.*
import ru.nkrus.telegram.alfabot.bot.Bot
import ru.nkrus.telegram.alfabot.model.Department
import ru.nkrus.telegram.alfabot.model.UserRole
import ru.nkrus.telegram.alfabot.repository.DepartmentRepository
import ru.nkrus.telegram.alfabot.repository.PersonRepository
import ru.nkrus.telegram.alfabot.service.NotificationService

@Component
class RegistrationHandler(
        val personRepository: PersonRepository,
        val departmentRepository: DepartmentRepository,
        val notificationService: NotificationService
) {
    val log = KotlinLogging.logger { }

    val groupRolesStates = mapOf(
            States.REGISTRATION_JOIN_AS_HEAD_OF_DEPARTMENT.name to UserRole.HEAD_OF_DEPARTMENT,
            States.REGISTRATION_JOIN_AS_COORDINATOR.name to UserRole.COORDINATOR,
            States.REGISTRATION_JOIN_AS_HEAD_OF_GROUP.name to UserRole.HEAD_OF_GROUP
    )

    fun handle(callbackQuery: CallbackQuery, session: Session, bot: Bot) {
        when {
            callbackQuery.data == States.REGISTRATION_SELECT_NAME.name -> askName(callbackQuery, session, bot)
            callbackQuery.data == States.REGISTRATION_SELECT_GROUP.name -> askDepartment(callbackQuery, session, bot)
            callbackQuery.data == States.REGISTRATION_SELECT_ROLE.name -> askRole(callbackQuery, session, bot)

            callbackQuery.data == States.REGISTRATION_CREATE_GROUP.name -> sayDepartmentCreated(callbackQuery, session, bot)
            callbackQuery.data == States.REGISTRATION_JOIN_GROUP.name -> askDepartmentId(callbackQuery, session, bot)
            callbackQuery.data in groupRolesStates.keys -> sayRoleChosen(callbackQuery, session, bot)
        }
    }

    fun handle(message: Message, session: Session, bot: Bot) {
        when {
            Commands.COMMAND_SET_NAME == session.getAttribute(Commands.SESSION_LAST_COMMAND) ->
                chooseName(message, session, bot)
            Commands.COMMAND_JOIN_GROUP == session.getAttribute(Commands.SESSION_LAST_COMMAND) ->
                chooseDepartmentId(message, session, bot)
        }
    }

    private fun chooseName(message: Message, session: Session, bot: Bot) {
        log.info { "Handling chooseName" }

        val person = personRepository.findPersonByChatId(message.chatId)!!
        person.name = message.text
        personRepository.save(person)

        session.removeAttribute(Commands.SESSION_LAST_COMMAND)

        bot.execute(SendMessage()
                .setChatId(message.chatId)
                .setReplyToMessageId(message.messageId)
                .setText("$DONE Отлично! Твои коллеги$PC_USER будут видеть тебя как: ${message.text}"))
        bot.execute(SendMessage()
                .setChatId(message.chatId)
                .setReplyMarkup(registrationInline(person))
                .setText(personStatusMessage(person)))
    }

    private fun chooseDepartmentId(message: Message, session: Session, bot: Bot) {
        log.info { "Handling chooseDepartmentId" }

        val person = personRepository.findPersonByChatId(message.chatId)!!
        val departmentId = message.text?.toLongOrNull()
        if (departmentId == null) {
            bot.execute(SendMessage()
                    .setChatId(message.chatId)
                    .setReplyToMessageId(message.messageId)
                    .setText("Не могу считать ID, это должно быть число."))
        } else {
            departmentRepository.findById(departmentId).ifPresentOrElse(
                    {
                        person.department = it
                        person.role = null
                        personRepository.save(person)
                        bot.execute(SendMessage()
                                .setChatId(message.chatId)
                                .setReplyToMessageId(message.messageId)
                                .setText("Я добавил тебя в группу с идентификатором: ${it.id}"))
                    },
                    {
                        bot.execute(SendMessage()
                                .setChatId(message.chatId)
                                .setReplyToMessageId(message.messageId)
                                .setText("Такой группы не существует."))
                    }
            )
        }
        session.removeAttribute(Commands.SESSION_LAST_COMMAND)
        bot.execute(SendMessage()
                .setChatId(message.chatId)
                .setReplyMarkup(registrationInline(person))
                .setText(personStatusMessage(person)))

    }

    private fun sayDepartmentCreated(callbackQuery: CallbackQuery, session: Session, bot: Bot) {
        log.info { "Handling sayDepartmentCreated" }

        val person = personRepository.findPersonByChatId(callbackQuery.message.chatId)!!
        val department = departmentRepository.save(Department())
        person.department = department
        personRepository.save(person)

        bot.execute(EditMessageText()
                .setChatId(callbackQuery.message.chatId)
                .setMessageId(callbackQuery.message.messageId)
                .setText("$DONE Новая группа создана. Ее уникальный идентификатор:\n${department.id}"))
        session.removeAttribute(Commands.SESSION_LAST_COMMAND)
        bot.execute(SendMessage()
                .setChatId(callbackQuery.message.chatId)
                .setReplyMarkup(registrationInline(person))
                .setText(personStatusMessage(person)))
    }

    private fun sayRoleChosen(callbackQuery: CallbackQuery, session: Session, bot: Bot) {
        log.info { "Handling sayRoleChosen" }

        val userRole = groupRolesStates[callbackQuery.data]
        val person = personRepository.findPersonByChatId(callbackQuery.message.chatId)!!
        val persons = person.department!!.persons

        if ((UserRole.HEAD_OF_DEPARTMENT == userRole && persons.any { it.role == UserRole.HEAD_OF_DEPARTMENT })
                || (UserRole.COORDINATOR == userRole && persons.any { it.role == UserRole.COORDINATOR })
                || (UserRole.HEAD_OF_GROUP == userRole && persons.filter { it.role == UserRole.HEAD_OF_GROUP }.count() >= 10)) {
            bot.execute(AnswerCallbackQuery()
                    .setCallbackQueryId(callbackQuery.id)
                    .setText("Нельзя, роль уже занята."))
            return
        }

        person.role = userRole
        personRepository.save(person)

        session.removeAttribute(Commands.SESSION_LAST_COMMAND)
        bot.execute(EditMessageText()
                .setChatId(callbackQuery.message.chatId)
                .setMessageId(callbackQuery.message.messageId)
                .setText("$DONE Окей, я присвоил тебе роль ${userRole?.title} $FINGER_UP"))
        bot.execute(SendMessage()
                .setChatId(callbackQuery.message.chatId)
                .setReplyMarkup(menuByRole(person.role!!))
                .setText(registrationCompleteStatus(person)))
        notificationService.sendNotification(Notifications.NEW_GROUP_MEMBER, person, bot)
    }

    private fun askDepartmentId(callbackQuery: CallbackQuery, session: Session?, bot: Bot) {
        log.info { "Handling askDepartmentId" }

        session?.setAttribute(Commands.SESSION_LAST_COMMAND, Commands.COMMAND_JOIN_GROUP)

        bot.execute(EditMessageText()
                .setChatId(callbackQuery.message.chatId)
                .setMessageId(callbackQuery.message.messageId)
                .setText("Напиши уникальный идентификатор группы, в которую хочешь вступить: "))
    }

    private fun askName(callbackQuery: CallbackQuery, session: Session?, bot: Bot) {
        log.info { "Handling askName" }

        session?.setAttribute(Commands.SESSION_LAST_COMMAND, Commands.COMMAND_SET_NAME)

        bot.execute(EditMessageText()
                .setChatId(callbackQuery.message.chatId)
                .setMessageId(callbackQuery.message.messageId)
                .setText("Окей, напиши свое имя и фамилию$ONE_EYE"))
    }

    private fun askDepartment(callbackQuery: CallbackQuery, session: Session?, bot: Bot) {
        log.info { "Handling askDepartment" }

        session?.setAttribute(Commands.SESSION_LAST_COMMAND, Commands.COMMAND_CHOOSE_GROUP)

        bot.execute(EditMessageText()
                .setChatId(callbackQuery.message.chatId)
                .setMessageId(callbackQuery.message.messageId)
                .setReplyMarkup(groupActionInline())
                .setText("Ты хочешь вступить в существующую группу или создать новую?$THINKING"))
    }

    private fun askRole(callbackQuery: CallbackQuery, session: Session?, bot: Bot) {
        log.info { "Handling askRole" }

        session?.setAttribute(Commands.SESSION_LAST_COMMAND, Commands.COMMAND_CHOOSE_ROLE)

        bot.execute(EditMessageText()
                .setChatId(callbackQuery.message.chatId)
                .setMessageId(callbackQuery.message.messageId)
                .setReplyMarkup(roleSelectionInline())
                .setText("Выбери свою роль в группе.$HEAD_DOWN"))
    }
}