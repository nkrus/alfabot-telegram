package ru.nkrus.telegram.alfabot.handler

import mu.KotlinLogging
import org.apache.shiro.session.Session
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Message
import ru.nkrus.telegram.alfabot.*
import ru.nkrus.telegram.alfabot.bot.Bot
import ru.nkrus.telegram.alfabot.model.Person
import ru.nkrus.telegram.alfabot.repository.PersonRepository
import java.util.*

@Component
class StartMessageHandler(
        val personRepository: PersonRepository
) {
    val log = KotlinLogging.logger { }

    fun handle(message: Message, botSession: Optional<Session>, bot: Bot) {
        log.info { "Handling start message" }
        var person = personRepository.findPersonByChatId(message.chatId)
        if (person?.role != null) {
            val sendMessage = SendMessage()
            sendMessage.enableMarkdown(true)
            sendMessage.setChatId(message.chatId)
            sendMessage.replyMarkup = menuByRole(person.role!!)
            sendMessage.text = "Выберите пункт меню"
            bot.execute(sendMessage)
        } else {
            if (person == null) {
                person = Person()
                person.chatId = message.chatId
                personRepository.save(person)
            }
            bot.execute(SendMessage()
                    .setChatId(message.chatId)
                    .setText(personStatusMessage(person))
                    .setReplyMarkup(registrationInline(person)))
        }
    }
}