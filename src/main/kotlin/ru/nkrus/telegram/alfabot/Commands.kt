package ru.nkrus.telegram.alfabot

enum class Commands(val text: String) {
    START_COMMAND("/start"),
    DELETE_ME("/deleteme"),

    COMMAND_CLEAR_PERSONAL_REPORTS("Очистить персональные отчеты $CLEANER"),
    SESSION_LAST_COMMAND("lastCommand"),
    SESSION_INSTEAD_CHAT_ID("insteadChatId"),

    //Roles
    SELECT_ROLE_HEAD_OF_DEPARTMENT("Руководитель отдела"),
    SELECT_ROLE_COORDINATOR("Координатор"),
    SELECT_ROLE_HEAD_OF_GROUP("Начальник группы"),

    //Registration
    COMMAND_SET_NAME("Выбрать имя"),
    COMMAND_CHOOSE_GROUP("Выбрать группу"),
    COMMAND_CHOOSE_ROLE("Выбрать роль"),

    COMMAND_CREATE_GROUP("Создать группу"),
    COMMAND_JOIN_GROUP("Вступить в группу"),

    //MainCommands
    COMMAND_ADD_BAW_REPORT("BAW отчет $BAW"),
    COMMAND_ADD_BAW_REPORT_NEW("Добавить BAW отчет"),

    COMMAND_PLAN("План $CALENDAR"),
    COMMAND_PLAN_NEW("Добавить план"),

    COMMAND_ADD_JBAW_REPORT("JBAW отчет $JBAW️"),
    COMMAND_ADD_JBAW_REPORT_NEW("Добавить JBAW отчет️"),

    COMMAND_GENERATE_REPORT("Свести отчет $GRAPHIC"),
    COMMAND_STATUS_DAY("Статус дня $STATUS"),
    COMMAND_REPORTS_HEAD_OF_GROUPS("Отчеты НГ $HEADS"),

    COMMAND_CHOOSE_HEAD_OF_GROUP("Выбрать начальника группы для редактирования отчета $BAW"),
    COMMAND_ADD_BAW_REPORT_NEW_INSTEAD("Подменить отчет $BAW"),

    COMMAND_SHARE_REPORT("Поделиться $SHARE")
}