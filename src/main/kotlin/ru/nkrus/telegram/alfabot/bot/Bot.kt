package ru.nkrus.telegram.alfabot.bot

import mu.KotlinLogging
import org.apache.shiro.session.Session
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Update
import org.telegram.telegrambots.session.TelegramLongPollingSessionBot
import ru.nkrus.telegram.alfabot.Commands.*
import ru.nkrus.telegram.alfabot.States
import ru.nkrus.telegram.alfabot.handler.DeleteMeHandler
import ru.nkrus.telegram.alfabot.handler.MainHandler
import ru.nkrus.telegram.alfabot.handler.StartMessageHandler
import ru.nkrus.telegram.alfabot.handler.RegistrationHandler
import java.util.*

@Component
class Bot(
        val startMessageHandler: StartMessageHandler,
        val registrationHandler: RegistrationHandler,
        val mainHandler: MainHandler,
        val deleteMeHandler: DeleteMeHandler
) : TelegramLongPollingSessionBot() {

    val log = KotlinLogging.logger { }

    val registrationCallbacks = listOf(States.REGISTRATION_SELECT_NAME.name,
            States.REGISTRATION_SELECT_GROUP.name, States.REGISTRATION_SELECT_ROLE.name, States.REGISTRATION_CREATE_GROUP.name,
            States.REGISTRATION_JOIN_GROUP.name, States.REGISTRATION_JOIN_AS_HEAD_OF_DEPARTMENT.name,
            States.REGISTRATION_JOIN_AS_COORDINATOR.name, States.REGISTRATION_JOIN_AS_HEAD_OF_GROUP.name)
    val registrationCommands = listOf(COMMAND_SET_NAME, COMMAND_CHOOSE_GROUP, COMMAND_CHOOSE_ROLE, COMMAND_CREATE_GROUP,
            COMMAND_JOIN_GROUP)
    val mainCommands = listOf(COMMAND_ADD_BAW_REPORT, COMMAND_ADD_JBAW_REPORT, COMMAND_GENERATE_REPORT, COMMAND_PLAN,
            COMMAND_STATUS_DAY, COMMAND_REPORTS_HEAD_OF_GROUPS, COMMAND_ADD_BAW_REPORT_NEW, COMMAND_PLAN_NEW, COMMAND_ADD_JBAW_REPORT_NEW,
            COMMAND_CHOOSE_HEAD_OF_GROUP, COMMAND_ADD_BAW_REPORT_NEW_INSTEAD, COMMAND_SHARE_REPORT)
    val mainCallbacks = listOf(States.BAW_NEW.name, States.PLAN_NEW.name, States.JBAW_NEW.name, States.SHARE_REPORT.name)

    @Value("\${bot.token}")
    lateinit var token: String

    @Value("\${bot.username}")
    lateinit var username: String

    override fun getBotUsername(): String = username

    override fun getBotToken(): String = token

    override fun onUpdateReceived(update: Update, botSession: Optional<Session>) {
        log.info("Got update: $update")
        if (update.hasMessage() && update.message.hasText()) {
            val message = update.message
            val text = message.text
            when {
                text?.startsWith(START_COMMAND.text) == true ->
                    startMessageHandler.handle(message, botSession, this)
                text?.startsWith(DELETE_ME.text) == true ->
                    deleteMeHandler.handle(message, this)
                (botSession.isPresent && botSession.get().getAttribute(SESSION_LAST_COMMAND) in registrationCommands) ->
                    registrationHandler.handle(message, botSession.get(), this)
                (botSession.isPresent && mainCommands.map { it.text }.contains(text)) -> mainHandler.handle(message, botSession.get(), this)
                (botSession.isPresent && botSession.get().getAttribute(SESSION_LAST_COMMAND) in mainCommands) ->
                    mainHandler.handle(message, botSession.get(), this)
                else -> startMessageHandler.handle(message, botSession, this)
            }
        } else if (update.hasCallbackQuery()) {
            val callbackQuery = update.callbackQuery
            when {
                (botSession.isPresent && callbackQuery.data in registrationCallbacks) ->
                    registrationHandler.handle(callbackQuery, botSession.get(), this)
                (botSession.isPresent && callbackQuery.data in mainCallbacks) ->
                    mainHandler.handle(callbackQuery, botSession.get(), this)
                (botSession.isPresent && botSession.get().getAttribute(SESSION_LAST_COMMAND) in mainCommands) ->
                    mainHandler.handle(callbackQuery, botSession.get(), this)
                else -> log.info { "No callback handler for data: ${callbackQuery.data}" }
            }
        }
    }

}