package ru.nkrus.telegram.alfabot.model.message

import ru.nkrus.telegram.alfabot.model.Department
import ru.nkrus.telegram.alfabot.util.currentDate
import java.time.LocalDate
import javax.persistence.*

/**
 * Ежедневный отчет координатора
 */
@Entity
class DailyJBAWReport(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,
        @Column
        var chatId: Long? = null,
        @Column
        var creationDate: LocalDate? = currentDate(),
        @Column
        var data: String? = null,
        @Column
        var debetCardsCountFact: Int = 0,
        @ManyToOne(fetch = FetchType.EAGER)
        @JoinColumn(name = "department_id")
        var department: Department? = null
) {
    override fun toString(): String = data.orEmpty()

}