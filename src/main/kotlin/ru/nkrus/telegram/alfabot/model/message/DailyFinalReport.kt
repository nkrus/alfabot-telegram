package ru.nkrus.telegram.alfabot.model.message

import ru.nkrus.telegram.alfabot.util.currentDate
import ru.nkrus.telegram.alfabot.util.dateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter

/**
 * Ежедневный финальный отчет
 */
data class DailyFinalReport(
        var creationDate: LocalDate = currentDate(),
        var creditCountPlan: Int = 0,
        var creditCountFact: Int = 0,
        var creditCardCountPlan: Int = 0,
        var creditCardCountFact: Int = 0,
        var debetCardCountFact: Int = 0,
        var creditApplicationCountFact: Int = 0,
        var creditCardApplicationCountFact: Int = 0,
        var totalCreditAndCreditCardPlan: Int = 0,
        var totalCreditAndCreditCardFact: Int = 0,
        var jBawData: String? = null
) {
    override fun toString(): String {
        return "❗Результат дня ${creationDate.format(dateFormat())} B@W Msk (план/факт):\n" +
                "Выдано:\n" +
                "PIL  $creditCountPlan  /  $creditCountFact\n" +
                "CC  $creditCardCountPlan  /  $creditCardCountFact\n" +
                "Total:  $totalCreditAndCreditCardPlan  /  $totalCreditAndCreditCardFact\n" +
                "DC (JB@W)  $debetCardCountFact\n" +
                "$jBawData\n" +
                "Заявки: \n" +
                "PIL $creditApplicationCountFact\n" +
                "CC  $creditCardApplicationCountFact"
    }
}