package ru.nkrus.telegram.alfabot.model

import javax.persistence.*

@Entity
class Department(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,
        @OneToMany(mappedBy = "department", fetch = FetchType.EAGER)
        var persons: List<Person> = mutableListOf()
)