package ru.nkrus.telegram.alfabot.model

import javax.persistence.*

@Entity
class Person(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,
        @Column
        var chatId: Long? = null,
        @Enumerated(EnumType.STRING)
        var role: UserRole? = null,
        @Column
        var name: String? = null,
        @ManyToOne(fetch = FetchType.EAGER)
        @JoinColumn(name = "department_id")
        var department: Department? = null
)

enum class UserRole(val title: String) {
    HEAD_OF_DEPARTMENT("Руководитель отдела"),
    COORDINATOR("Координатор"),
    HEAD_OF_GROUP("Начальник группы");
}