package ru.nkrus.telegram.alfabot.model.message

import ru.nkrus.telegram.alfabot.model.Department
import ru.nkrus.telegram.alfabot.model.Person
import ru.nkrus.telegram.alfabot.util.currentDate
import ru.nkrus.telegram.alfabot.util.dateFormat
import java.time.LocalDate
import javax.persistence.*

@Entity
class DailyPlan(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,
        @Column
        var chatId: Long? = null,
        @Column
        var creationDate: LocalDate? = currentDate(),

        @Column
        var creditCountPlan: Int = 0,
        @Column
        var creditCardCountPlan: Int = 0,
        @ManyToOne(fetch = FetchType.EAGER)
        @JoinColumn(name = "department_id")
        var department: Department? = null,
        @ManyToOne(fetch = FetchType.EAGER)
        @JoinColumn(name = "person_id")
        var person: Person? = null

) {
    override fun toString(): String {
        return "Дата создания=${creationDate?.format(dateFormat())}\n" +
                "PIL=$creditCountPlan\n" +
                "CC=$creditCardCountPlan\n"
    }
}