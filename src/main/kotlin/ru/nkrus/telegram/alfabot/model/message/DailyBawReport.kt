package ru.nkrus.telegram.alfabot.model.message

import ru.nkrus.telegram.alfabot.model.Department
import ru.nkrus.telegram.alfabot.util.currentDate
import ru.nkrus.telegram.alfabot.util.dateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import javax.persistence.*

/**
 * Ежедневный отчет начальника группы
 */
@Entity
class DailyBawReport(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,
        @Column
        var chatId: Long? = null,
        @Column
        var creationDate: LocalDate? = currentDate(),
        @Column
        var creditCountPlan: Int = 0,
        @Column
        var creditCountFact: Int = 0,
        @Column
        var creditApplicationCountFact: Int = 0,
        @Column
        var creditCardCountFact: Int = 0,
        @Column
        var creditCardApplicationCountFact: Int = 0,
        @Column
        var creditCardCountPlan: Int = 0,
        @Column
        var bankActivity: Int = 0,
        @ManyToOne(fetch = FetchType.EAGER)
        @JoinColumn(name = "department_id")
        var department: Department? = null
) {
    override fun toString(): String {
        return "Дата создания:${creationDate?.format(dateFormat())}\n" +
                "PIL $creditCountPlan/$creditCountFact/$creditApplicationCountFact\n" +
                "CC $creditCardCountPlan/$creditCardCountFact/$creditCardApplicationCountFact\n" +
                "Total: ${creditCountFact + creditCardCountFact}\n" +
                "БА: $bankActivity"
    }
}