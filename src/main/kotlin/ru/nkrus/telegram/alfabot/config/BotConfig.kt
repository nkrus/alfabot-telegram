package ru.nkrus.telegram.alfabot.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "bot")
class BotConfig {
    lateinit var token: String
    lateinit var username: String
    lateinit var telegramId: String
}
