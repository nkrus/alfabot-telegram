package ru.nkrus.telegram.alfabot.service

import mu.KotlinLogging
import org.springframework.stereotype.Service
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import ru.nkrus.telegram.alfabot.NOTIFIC
import ru.nkrus.telegram.alfabot.Notifications
import ru.nkrus.telegram.alfabot.bot.Bot
import ru.nkrus.telegram.alfabot.model.Person
import ru.nkrus.telegram.alfabot.model.UserRole

@Service
class NotificationService {

    val log = KotlinLogging.logger { }

    fun sendNotification(notification: Notifications, from: Person, bot: Bot, message: String = "", instead: Person? = null) {
        log.info { "Created notification: $notification, from chatId:${from.chatId}" }
        when (notification) {
            Notifications.NEW_DAILY_BAW_REPORT -> notify("${notification.message}\n$message", from, bot, notification.roles)
            Notifications.NEW_DAILY_PLAN -> notify("${notification.message}\n$message", from, bot, notification.roles)
            Notifications.NEW_DAILY_JBAW_REPORT -> notify(notification.message, from, bot, notification.roles)
            Notifications.REPORT_CREATED -> notify("${notification.message}\n$message", from, bot, notification.roles)
            Notifications.NEW_DAILY_BAW_REPORT_INSTEAD -> notify("${notification.message}\n$message", from, bot, notification.roles, instead = instead)
            Notifications.NEW_GROUP_MEMBER -> notify("${from.role?.title} - ${notification.message}", from, bot, notification.roles)
            Notifications.PERSON_DELETED -> notify("${from.role?.title} - ${notification.message}", from, bot, notification.roles)
        }
    }

    private fun notify(message: String, from: Person, bot: Bot, roles: List<UserRole>, instead: Person? = null) {
        from.department?.persons
                ?.filter { it.id != from.id }
                ?.filter { it.role in roles }
                ?.forEach {
                    log.info { "Sending notification to ${it.chatId}" }
                    bot.execute(SendMessage()
                            .setChatId(it.chatId)
                            .setText("$NOTIFIC${from.name}" +
                                    instead?.name?.let { name -> " - (Вместо: $name" }.orEmpty() +
                                    " - $message"))
                }
    }
}