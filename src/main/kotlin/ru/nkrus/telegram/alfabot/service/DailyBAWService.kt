package ru.nkrus.telegram.alfabot.service

import org.springframework.stereotype.Service
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Message
import ru.nkrus.telegram.alfabot.Commands
import ru.nkrus.telegram.alfabot.model.Department
import ru.nkrus.telegram.alfabot.model.message.DailyBawReport
import ru.nkrus.telegram.alfabot.repository.DailyBawDataRepository
import java.time.LocalDate

@Service
class DailyBAWService(val dailyBawDataRepository: DailyBawDataRepository) {

    fun save(dailyBawReport: DailyBawReport): DailyBawReport {
        return dailyBawDataRepository.save(dailyBawReport)
    }

    fun deleteByChatIdAndCreationDate(chatId: Long, date: LocalDate) {
        findByChatIdAndDate(chatId, date)?.let { dailyBawDataRepository.delete(it) }
    }

    fun findAllByDepartmentAndAndCreationDate(department: Department, date: LocalDate) =
            dailyBawDataRepository.findAllByDepartmentAndAndCreationDate(department, date)

    fun findByChatIdAndDate(chatId: Long, date: LocalDate) =
            dailyBawDataRepository.findByChatIdAndCreationDate(chatId, date)

    fun findByDepartmentAndCreationDate(department: Department, date: LocalDate) =
            dailyBawDataRepository.findByDepartmentAndCreationDate(department, date)
}