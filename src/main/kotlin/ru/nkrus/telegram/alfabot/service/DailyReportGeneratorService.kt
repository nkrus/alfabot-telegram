package ru.nkrus.telegram.alfabot.service

import org.springframework.stereotype.Service
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton
import ru.nkrus.telegram.alfabot.BAW
import ru.nkrus.telegram.alfabot.Commands
import ru.nkrus.telegram.alfabot.States
import ru.nkrus.telegram.alfabot.model.Department
import ru.nkrus.telegram.alfabot.model.message.DailyFinalReport
import ru.nkrus.telegram.alfabot.util.currentDate
import java.time.LocalDate

@Service
class DailyReportGeneratorService(
        val dailyBAWService: DailyBAWService,
        val dailyJbawService: DailyJbawService,
        val dailyPlanService: DailyPlanService
) {

    fun generateReport(department: Department, date: LocalDate): SendMessage {
        val response = SendMessage()
        val dailyPlan = dailyPlanService.findByDepartmentAndCreationDate(department, date)
        if (dailyPlan == null) {
            response.text = "Сначала нужно заполнить план на день!\n Нажми кнопку ${Commands.COMMAND_PLAN.text}"
            return response
        }
        val dailyJbaw = dailyJbawService.findByDepartmentAndCreationDate(department, currentDate())
        val records = dailyBAWService.findAllByDepartmentAndAndCreationDate(department, currentDate())
        if (records.isEmpty()) {
            response.text = "Еще не могу свести отчет.\nНужно занести хотя бы один BAW $BAW отчет в группу."
            return response
        }

        val dailyReport = DailyFinalReport(
                jBawData = dailyJbaw?.data ?: "",
                creditCountPlan = dailyPlan.creditCountPlan,
                creditCountFact = records.map { it.creditCountFact }.reduce { acc, count -> acc + count },
                creditCardCountPlan = dailyPlan.creditCardCountPlan,
                creditCardCountFact = records.map { it.creditCardCountFact }.reduce { acc, count -> acc + count },
                debetCardCountFact = dailyJbaw?.debetCardsCountFact ?: 0,
                creditApplicationCountFact = records.map { it.creditApplicationCountFact }.reduce { acc, count -> acc + count },
                creditCardApplicationCountFact = records.map { it.creditCardApplicationCountFact }.reduce { acc, count -> acc + count }
        )
                .also {
                    it.totalCreditAndCreditCardPlan = it.creditCountPlan + it.creditCardCountPlan
                    it.totalCreditAndCreditCardFact = it.creditCountFact + it.creditCardCountFact
                }

        response.text = dailyReport.toString()
        response.replyMarkup = InlineKeyboardMarkup().setKeyboard((listOf(
                listOf<InlineKeyboardButton>(
                        InlineKeyboardButton().setText(Commands.COMMAND_SHARE_REPORT.text)
                                .setCallbackData(States.SHARE_REPORT.name)
                )
        )))
        return response
    }
}