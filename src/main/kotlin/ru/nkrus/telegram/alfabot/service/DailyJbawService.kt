package ru.nkrus.telegram.alfabot.service

import org.springframework.stereotype.Service
import ru.nkrus.telegram.alfabot.model.Department
import ru.nkrus.telegram.alfabot.model.message.DailyJBAWReport
import ru.nkrus.telegram.alfabot.repository.DailyJbawRepository
import java.time.LocalDate

@Service
class DailyJbawService(val dailyJbawRepository: DailyJbawRepository) {

    fun save(dailyBawReport: DailyJBAWReport): DailyJBAWReport {
        return dailyJbawRepository.save(dailyBawReport)
    }

    fun deleteByChatIdAndCreationDate(chatId: Long, date: LocalDate) {
        findByChatIdAndDate(chatId, date)?.let { dailyJbawRepository.delete(it) }
    }


    fun findByChatIdAndDate(chatId: Long, date: LocalDate) =
            dailyJbawRepository.findByChatIdAndCreationDate(chatId, date)

    fun findByDepartmentAndCreationDate(department: Department, date: LocalDate) =
            dailyJbawRepository.findByDepartmentAndCreationDate(department, date)
}