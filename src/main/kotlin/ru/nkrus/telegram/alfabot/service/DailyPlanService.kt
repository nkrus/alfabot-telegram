package ru.nkrus.telegram.alfabot.service

import org.springframework.stereotype.Service
import ru.nkrus.telegram.alfabot.model.Department
import ru.nkrus.telegram.alfabot.model.message.DailyPlan
import ru.nkrus.telegram.alfabot.repository.DailyPlanRepository
import java.time.LocalDate

@Service
class DailyPlanService(
        val dailyPlanRepository: DailyPlanRepository
) {

    fun save(dailyPlan: DailyPlan) = dailyPlanRepository.save(dailyPlan)

    fun findByDepartmentAndCreationDate(department: Department, date: LocalDate) =
            dailyPlanRepository.findByDepartmentAndCreationDate(department, date)

    fun deleteByDepartmentAndCreationDate(department: Department, date: LocalDate) =
            findByDepartmentAndCreationDate(department, date)?.let { dailyPlanRepository.delete(it) }

}