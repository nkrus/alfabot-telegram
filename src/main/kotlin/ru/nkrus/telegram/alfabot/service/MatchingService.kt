package ru.nkrus.telegram.alfabot.service

import mu.KotlinLogging
import org.ahocorasick.trie.Emit
import org.springframework.stereotype.Service
import org.telegram.telegrambots.meta.api.objects.Message
import ru.nkrus.telegram.alfabot.model.message.DailyBawReport
import ru.nkrus.telegram.alfabot.model.message.DailyJBAWReport
import ru.nkrus.telegram.alfabot.model.message.DailyPlan
import ru.nkrus.telegram.alfabot.util.VariablesMatch
import ru.nkrus.telegram.alfabot.repository.DailyBawDataRepository
import ru.nkrus.telegram.alfabot.repository.DailyJbawRepository
import ru.nkrus.telegram.alfabot.repository.DailyPlanRepository

@Service
class MatchingService(
        val dailyPlanRepository: DailyPlanRepository,
        val dailyBawDataRepository: DailyBawDataRepository,
        val dailyJbawRepository: DailyJbawRepository
) {

    val log = KotlinLogging.logger { }

    fun matchDailyBaw(message: Message): DailyBawReport {
        log.info("Input string for matching daily personal data: ${message.text}")
        val dailyBaw = DailyBawReport()
        message.text.lines().forEach {
            when {
                VariablesMatch.CREDITS_COUNT.trie.containsMatch(it) -> {
                    VariablesMatch.CREDITS_COUNT.trie.firstMatch(it)?.let(fun(emit: Emit) {
                        val values = it.substring(emit.end)
                                .replace("[^0-9]".toRegex(), " ")
                                .replace("\\s+".toRegex(), " ")
                                .trim()
                                .split(" ")
                        when {
                            values.size == 3 -> {
                                log.info("Fill credit information")
                                dailyBaw.creditCountPlan = values[0].toInt()
                                dailyBaw.creditCountFact = values[1].toInt()
                                dailyBaw.creditApplicationCountFact = values[2].toInt()
                            }
                            values.size == 2 -> {
                                log.info("Fill credit information")
                                dailyBaw.creditCountPlan = values[0].toInt()
                                dailyBaw.creditCountFact = values[1].toInt()
                            }
                            values.size == 1 -> {
                                log.info("Fill credit information")
                                dailyBaw.creditCountFact = values[0].toInt()
                            }
                        }
                    })
                }
                VariablesMatch.CREDIT_CARDS_COUNT.trie.containsMatch(it) -> {
                    VariablesMatch.CREDIT_CARDS_COUNT.trie.firstMatch(it)?.let(fun(emit: Emit) {
                        val values = it.substring(emit.end)
                                .replace("[^0-9]".toRegex(), " ")
                                .replace("\\s+".toRegex(), " ")
                                .trim()
                                .split(" ")
                        when {
                            values.size == 3 -> {
                                log.info("Fill credit card information")
                                dailyBaw.creditCardCountPlan = values[0].toInt()
                                dailyBaw.creditCardCountFact = values[1].toInt()
                                dailyBaw.creditCardApplicationCountFact = values[2].toInt()
                            }
                            values.size == 2 -> {
                                log.info("Fill credit card information")
                                dailyBaw.creditCardCountPlan = values[0].toInt()
                                dailyBaw.creditCardCountFact = values[1].toInt()
                            }
                            values.size == 1 -> {
                                log.info("Fill credit card information")
                                dailyBaw.creditCardCountFact = values[0].toInt()
                            }
                        }
                    })
                }
                VariablesMatch.BANK_ACTIVITY.trie.containsMatch(it) -> {
                    VariablesMatch.BANK_ACTIVITY.trie.firstMatch(it)?.let(fun(emit: Emit) {
                        val values = it.substring(emit.end)
                                .replace("[^0-9]".toRegex(), " ")
                                .replace("\\s+".toRegex(), " ")
                                .trim()
                                .split(" ")
                        if (values.size == 1) {
                            log.info("Fill bank activity information")
                            dailyBaw.bankActivity = values[0].toInt()
                        }
                    })
                }
            }
        }
        dailyBaw.chatId = message.chatId
        return dailyBawDataRepository.save(dailyBaw)
    }

    fun matchDailyPlan(message: Message): DailyPlan {
        log.info("Input string for matching daily plan: ${message.text}")
        val dailyPlan = DailyPlan()
        message.text.lines().forEach {
            when {
                VariablesMatch.CREDITS_COUNT.trie.containsMatch(it) -> {
                    VariablesMatch.CREDITS_COUNT.trie.firstMatch(it)?.let(fun(emit: Emit) {
                        it.substring(emit.end)
                                .replace("[^0-9]".toRegex(), "")
                                .trim()
                                .let { value ->
                                    log.info("Fill credit information")
                                    dailyPlan.creditCountPlan = value.toInt()
                                }
                    })
                }
                VariablesMatch.CREDIT_CARDS_COUNT.trie.containsMatch(it) -> {
                    VariablesMatch.CREDIT_CARDS_COUNT.trie.firstMatch(it)?.let(fun(emit: Emit) {
                        it.substring(emit.end)
                                .replace("[^0-9]".toRegex(), "")
                                .trim()
                                .let { value ->
                                    log.info("Fill credit card information")
                                    dailyPlan.creditCardCountPlan = value.toInt()
                                }
                    })
                }
            }
        }
        dailyPlan.chatId = message.chatId
        return dailyPlanRepository.save(dailyPlan)
    }

    fun matchJbawReport(message: Message): DailyJBAWReport {
        log.info("Input string for matching daily JBAW report: ${message.text}")
        val dailyJBAWReport = DailyJBAWReport()
        message.text.lines().forEach {
            when {
                VariablesMatch.DEBIT_CARDS_COUNT.trie.containsMatch(it) -> {
                    VariablesMatch.DEBIT_CARDS_COUNT.trie.firstMatch(it)?.let(fun(emit: Emit) {
                        val values = it.substring(emit.end)
                                .replace("[^0-9]".toRegex(), " ")
                                .replace("\\s+".toRegex(), " ")
                                .trim()
                                .split(" ")
                        when {
                            values.size >= 2 -> {
                                log.info("Fill DC information")
                                dailyJBAWReport.debetCardsCountFact = values[1].toInt()
                            }
                        }
                    })
                }
            }
        }
        dailyJBAWReport.data = message.text
        dailyJBAWReport.chatId = message.chatId
        return dailyJbawRepository.save(dailyJBAWReport)
    }
}
