package ru.nkrus.telegram.alfabot.util

import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter

fun currentDate(): LocalDate = LocalDate.now(ZoneId.of("Europe/Moscow"))

fun dateFormat(): DateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy")
