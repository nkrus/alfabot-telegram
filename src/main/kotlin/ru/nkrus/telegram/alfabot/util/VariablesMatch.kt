package ru.nkrus.telegram.alfabot.util

import org.ahocorasick.trie.Trie

enum class VariablesMatch(val title: String, val trie: Trie) {
    CREDITS_COUNT("Кредит",
            Trie.builder()
                    .ignoreCase()
                    .ignoreOverlaps()
                    .onlyWholeWords()
                    .addKeyword("Pil")
                    .addKeyword("Пил")
                    .addKeyword("Кредит")
                    .build()),

    CREDIT_CARDS_COUNT("Кредитные карты",
            Trie.builder()
                    .ignoreCase()
                    .ignoreOverlaps()
                    .onlyWholeWords()
                    .addKeyword("CC")
                    .addKeyword("СС")
                    .addKeyword("KK")
                    .addKeyword("КК")
                    .build()),
    DEBIT_CARDS_COUNT("DC",
            Trie.builder()
                    .ignoreCase()
                    .ignoreOverlaps()
                    .onlyWholeWords()
                    .addKeyword("Выдач")
                    .build()),
    BANK_ACTIVITY("БА",
            Trie.builder()
                    .ignoreCase()
                    .ignoreOverlaps()
                    .onlyWholeWords()
                    .addKeyword("БА")
                    .build())
}