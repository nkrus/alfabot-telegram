package ru.nkrus.telegram.alfabot

import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow
import ru.nkrus.telegram.alfabot.Commands.*
import ru.nkrus.telegram.alfabot.model.Person
import ru.nkrus.telegram.alfabot.model.UserRole
import ru.nkrus.telegram.alfabot.model.message.DailyBawReport
import ru.nkrus.telegram.alfabot.model.message.DailyJBAWReport
import ru.nkrus.telegram.alfabot.model.message.DailyPlan
import ru.nkrus.telegram.alfabot.util.currentDate
import ru.nkrus.telegram.alfabot.util.dateFormat


fun menuByRole(userRole: UserRole): ReplyKeyboardMarkup {
    return when (userRole) {
        UserRole.HEAD_OF_DEPARTMENT -> headOfDepartmentMenu()
        UserRole.COORDINATOR -> coordinatorMenu()
        UserRole.HEAD_OF_GROUP -> headOfGroupMenu()
    }
}

fun headOfDepartmentMenu(): ReplyKeyboardMarkup {
    val replyKeyboardMarkup = ReplyKeyboardMarkup()
    replyKeyboardMarkup.selective = true
    replyKeyboardMarkup.resizeKeyboard = true
    replyKeyboardMarkup.oneTimeKeyboard = false
    replyKeyboardMarkup.keyboard = listOf(
            KeyboardRow().also { it.add(COMMAND_PLAN.text) },
            KeyboardRow().also { it.add(COMMAND_STATUS_DAY.text) },
            KeyboardRow().also { it.add(COMMAND_GENERATE_REPORT.text) },
            KeyboardRow().also { it.add(COMMAND_REPORTS_HEAD_OF_GROUPS.text) }
    )

    return replyKeyboardMarkup
}

fun coordinatorMenu(): ReplyKeyboardMarkup {
    val replyKeyboardMarkup = ReplyKeyboardMarkup()
    replyKeyboardMarkup.selective = true
    replyKeyboardMarkup.resizeKeyboard = true
    replyKeyboardMarkup.oneTimeKeyboard = false
    replyKeyboardMarkup.keyboard = listOf(
            KeyboardRow().also { it.add(COMMAND_PLAN.text) },
            KeyboardRow().also { it.add(COMMAND_ADD_JBAW_REPORT.text) },
            KeyboardRow().also { it.add(COMMAND_GENERATE_REPORT.text) },
            KeyboardRow().also { it.add(COMMAND_STATUS_DAY.text) },
            KeyboardRow().also { it.add(COMMAND_REPORTS_HEAD_OF_GROUPS.text) }

    )

    return replyKeyboardMarkup
}


fun headOfGroupMenu(): ReplyKeyboardMarkup {
    val replyKeyboardMarkup = ReplyKeyboardMarkup()
    replyKeyboardMarkup.selective = true
    replyKeyboardMarkup.resizeKeyboard = true
    replyKeyboardMarkup.oneTimeKeyboard = false
    replyKeyboardMarkup.keyboard = listOf(
            KeyboardRow().also { it.add(COMMAND_ADD_BAW_REPORT.text) }

    )

    return replyKeyboardMarkup
}

fun registrationInline(person: Person?): InlineKeyboardMarkup {
    val markupInline = InlineKeyboardMarkup()
    markupInline.keyboard = mutableListOf(
            listOf<InlineKeyboardButton>(
                    InlineKeyboardButton().setText(COMMAND_SET_NAME.text)
                            .setCallbackData(States.REGISTRATION_SELECT_NAME.name)
            )
    )
    person?.name?.also {
        markupInline.keyboard.add(
                listOf<InlineKeyboardButton>(
                        InlineKeyboardButton().setText(COMMAND_CHOOSE_GROUP.text)
                                .setCallbackData(States.REGISTRATION_SELECT_GROUP.name)
                ))
    }
    person?.department?.also {
        markupInline.keyboard.add(
                listOf<InlineKeyboardButton>(
                        InlineKeyboardButton().setText(COMMAND_CHOOSE_ROLE.text)
                                .setCallbackData(States.REGISTRATION_SELECT_ROLE.name)
                ))
    }
    return markupInline
}

fun groupActionInline(): InlineKeyboardMarkup {
    val markupInline = InlineKeyboardMarkup()
    markupInline.keyboard = listOf(
            listOf<InlineKeyboardButton>(
                    InlineKeyboardButton().setText(COMMAND_CREATE_GROUP.text)
                            .setCallbackData(States.REGISTRATION_CREATE_GROUP.name),
                    InlineKeyboardButton().setText(COMMAND_JOIN_GROUP.text)
                            .setCallbackData(States.REGISTRATION_JOIN_GROUP.name)
            )
    )
    return markupInline
}

fun personStatusMessage(person: Person?): String =
        "Привет$HI_HAND. Чтобы начать пользоваться ботом необходимо зарегистрироваться!\n\n" +
                "Ваш процесс регистрации: \n" +
                "Имя: ${person?.name ?: CANCEL}\n" +
                "Группа: ${person?.department?.id ?: CANCEL}\n" +
                "Роль: ${person?.role?.title ?: CANCEL}"

fun registrationCompleteStatus(person: Person?): String =
        "$DONE Я успешно зарегистрировал тебя:\n" +
                "Имя: ${person?.name ?: CANCEL}\n" +
                "Группа: ${person?.department?.id ?: CANCEL}\n" +
                "Роль: ${person?.role?.title ?: CANCEL}"

fun roleSelectionInline(): InlineKeyboardMarkup {
    val markupInline = InlineKeyboardMarkup()
    markupInline.keyboard = listOf(
            listOf<InlineKeyboardButton>(
                    InlineKeyboardButton().setText(SELECT_ROLE_HEAD_OF_DEPARTMENT.text)
                            .setCallbackData(States.REGISTRATION_JOIN_AS_HEAD_OF_DEPARTMENT.name)
            ),
            listOf<InlineKeyboardButton>(
                    InlineKeyboardButton().setText(SELECT_ROLE_COORDINATOR.text)
                            .setCallbackData(States.REGISTRATION_JOIN_AS_COORDINATOR.name)
            ),
            listOf<InlineKeyboardButton>(
                    InlineKeyboardButton().setText(SELECT_ROLE_HEAD_OF_GROUP.text)
                            .setCallbackData(States.REGISTRATION_JOIN_AS_HEAD_OF_GROUP.name)
            )
    )
    return markupInline
}

fun bawReport(bawReport: DailyBawReport?): SendMessage {
    val message = SendMessage()
    val markupInline = InlineKeyboardMarkup().setKeyboard((listOf(
            listOf<InlineKeyboardButton>(
                    InlineKeyboardButton().setText(COMMAND_ADD_BAW_REPORT_NEW.text)
                            .setCallbackData(States.BAW_NEW.name)
            )
    )))
    return message
            .setText(
                    if (bawReport == null) "${NOT_DONE}У тебя нет отчета за сегодня. Чтобы прислать отчет нажми ${COMMAND_ADD_BAW_REPORT_NEW.text}"
                    else "$DONE Отчет сегодня уже добавлен\n$bawReport \nЕсли хочешь изменить отчет, нажми ${COMMAND_ADD_BAW_REPORT_NEW.text}"
            )
            .setReplyMarkup(markupInline)
}

fun jBawReport(jbawReport: DailyJBAWReport?): SendMessage {
    val message = SendMessage()
    val markupInline = InlineKeyboardMarkup().setKeyboard((listOf(
            listOf<InlineKeyboardButton>(
                    InlineKeyboardButton().setText(COMMAND_ADD_JBAW_REPORT_NEW.text)
                            .setCallbackData(States.JBAW_NEW.name)
            )
    )))
    return message
            .setText(
                    if (jbawReport == null) "${NOT_DONE}У тебя нет JBAW отчета за сегодня. Чтобы прислать отчет нажми ${COMMAND_ADD_JBAW_REPORT_NEW.text}"
                    else "$DONE Отчет JBAW сегодня уже добавлен\n$jbawReport \nЕсли хочешь изменить отчет, нажми ${COMMAND_ADD_JBAW_REPORT_NEW.text}"
            )
            .setReplyMarkup(markupInline)
}

fun dailyPlan(dailyPlan: DailyPlan?): SendMessage {
    val message = SendMessage()
    val markupInline = InlineKeyboardMarkup().setKeyboard((listOf(
            listOf<InlineKeyboardButton>(
                    InlineKeyboardButton().setText(COMMAND_PLAN_NEW.text)
                            .setCallbackData(States.PLAN_NEW.name)
            )
    )))
    return message
            .setText(
                    if (dailyPlan == null) "${NOT_DONE}План на сегодня еще не добавлен. Чтобы прислать план нажми ${COMMAND_PLAN_NEW.text}"
                    else "$DONE План сегодня уже добавлен, пользователем - ${dailyPlan.person?.name}\n$dailyPlan \nЕсли хочешь изменить план, нажми ${COMMAND_PLAN_NEW.text}"
            )
            .setReplyMarkup(markupInline)
}

fun statusDay(dailyPlan: DailyPlan?, dailyJbaw: DailyJBAWReport?, bawReportStatusByPersonName: List<Pair<String?, Boolean>>): SendMessage =
        SendMessage()
                .setText(
                        "Статус - ${currentDate().format(dateFormat())}\n" +
                                "План - ${if (dailyPlan == null) NOT_DONE else DONE}\n" +
                                "JBAW отчет - ${if (dailyJbaw == null) NOT_DONE else DONE}\n" +
                                "BAW отчеты:\n" +
                                bawReportStatusByPersonName
                                        .joinToString("\n") { "${it.first} - ${if (it.second) DONE else NOT_DONE}" }
                )

fun headOfGroupsBawReports(persons: List<Pair<Person, Boolean>>): SendMessage = SendMessage()
        .setText("Отчеты НГ за ${currentDate().format(dateFormat())}")
        .setReplyMarkup(InlineKeyboardMarkup().setKeyboard((
                persons.map {
                    listOf(InlineKeyboardButton().setText("${it.first.name} ${if (it.second) DONE else NOT_DONE}")
                            .setCallbackData(it.first.chatId.toString()))
                }))
        )


fun bawReportInstead(bawReport: DailyBawReport?, chatId: String, person: Person?): EditMessageText {
    val message = EditMessageText()
    val markupInline = InlineKeyboardMarkup().setKeyboard((listOf(
            listOf<InlineKeyboardButton>(
                    InlineKeyboardButton().setText(COMMAND_ADD_BAW_REPORT_NEW_INSTEAD.text)
                            .setCallbackData(chatId)
            )
    )))
    return message
            .setText(
                    if (bawReport == null) "${person?.name.orEmpty()}\n${NOT_DONE}BAW отчета нет. Чтобы добавить отчет нажми ${COMMAND_ADD_BAW_REPORT_NEW_INSTEAD.text}"
                    else "${person?.name.orEmpty()}\n$DONE Отчет сегодня уже добавлен\n$bawReport \nЕсли хочешь изменить отчет, нажми ${COMMAND_ADD_BAW_REPORT_NEW_INSTEAD.text}"
            )
            .setReplyMarkup(markupInline)
}

