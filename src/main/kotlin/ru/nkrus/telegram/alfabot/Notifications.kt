package ru.nkrus.telegram.alfabot

import ru.nkrus.telegram.alfabot.model.UserRole

enum class Notifications(val message: String, val roles: List<UserRole>) {
    NEW_DAILY_PLAN("Дневной план добавлен",
            listOf(UserRole.HEAD_OF_DEPARTMENT, UserRole.COORDINATOR, UserRole.HEAD_OF_GROUP)),
    NEW_DAILY_BAW_REPORT("BAW отчет добавлен",
            listOf(UserRole.HEAD_OF_DEPARTMENT, UserRole.COORDINATOR, UserRole.HEAD_OF_GROUP)),
    NEW_DAILY_BAW_REPORT_INSTEAD("BAW отчет добавлен",
            listOf(UserRole.HEAD_OF_DEPARTMENT, UserRole.COORDINATOR, UserRole.HEAD_OF_GROUP)),
    NEW_DAILY_JBAW_REPORT("JBAW отчет добавлен",
            listOf(UserRole.HEAD_OF_DEPARTMENT)),
    NEW_GROUP_MEMBER("Новый член группы",
            listOf(UserRole.HEAD_OF_DEPARTMENT, UserRole.HEAD_OF_GROUP, UserRole.COORDINATOR)),
    PERSON_DELETED("Удалил себя из группы",
            listOf(UserRole.HEAD_OF_DEPARTMENT, UserRole.HEAD_OF_GROUP, UserRole.COORDINATOR)),
    REPORT_CREATED("Отчет по дню сведен",
            listOf(UserRole.HEAD_OF_DEPARTMENT, UserRole.HEAD_OF_GROUP, UserRole.COORDINATOR))

}