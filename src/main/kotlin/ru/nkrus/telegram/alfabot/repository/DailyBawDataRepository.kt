package ru.nkrus.telegram.alfabot.repository

import org.springframework.data.repository.CrudRepository
import ru.nkrus.telegram.alfabot.model.Department
import ru.nkrus.telegram.alfabot.model.message.DailyBawReport
import java.time.LocalDate

interface DailyBawDataRepository : CrudRepository<DailyBawReport, Long> {
    fun findAllByDepartmentAndAndCreationDate(department: Department, creationDate: LocalDate): List<DailyBawReport>
    fun findByChatIdAndCreationDate(chatId: Long, creationDate: LocalDate): DailyBawReport?
    fun findByDepartmentAndCreationDate(department: Department, creationDate: LocalDate): DailyBawReport?
    fun deleteAllByChatId(chatId: Long)

}