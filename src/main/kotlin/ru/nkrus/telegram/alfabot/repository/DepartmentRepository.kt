package ru.nkrus.telegram.alfabot.repository

import org.springframework.data.repository.CrudRepository
import ru.nkrus.telegram.alfabot.model.Department

interface DepartmentRepository : CrudRepository<Department, Long> {
}