package ru.nkrus.telegram.alfabot.repository

import org.springframework.data.repository.CrudRepository
import ru.nkrus.telegram.alfabot.model.Department
import ru.nkrus.telegram.alfabot.model.message.DailyPlan
import java.time.LocalDate

interface DailyPlanRepository : CrudRepository<DailyPlan, Long> {
    fun findByDepartmentAndCreationDate(department: Department, creationDate: LocalDate): DailyPlan?
    fun deleteAllByChatId(chatId: Long)
}