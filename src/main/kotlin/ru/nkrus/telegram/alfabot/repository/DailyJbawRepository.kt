package ru.nkrus.telegram.alfabot.repository

import org.springframework.data.repository.CrudRepository
import ru.nkrus.telegram.alfabot.model.Department
import ru.nkrus.telegram.alfabot.model.message.DailyJBAWReport
import java.time.LocalDate

interface DailyJbawRepository : CrudRepository<DailyJBAWReport, Long> {
    fun findByChatIdAndCreationDate(chatId: Long, creationDate: LocalDate): DailyJBAWReport?
    fun findByDepartmentAndCreationDate(department: Department, creationDate: LocalDate): DailyJBAWReport?
    fun deleteAllByChatId(chatId: Long)

}