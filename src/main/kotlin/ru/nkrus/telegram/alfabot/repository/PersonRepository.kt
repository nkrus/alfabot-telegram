package ru.nkrus.telegram.alfabot.repository

import org.springframework.data.repository.CrudRepository
import ru.nkrus.telegram.alfabot.model.Person

interface PersonRepository : CrudRepository<Person, Long> {
    fun findPersonByChatId(chatId: Long): Person?
}