FROM openjdk:11.0.5-jre-stretch
EXPOSE 13789
ADD /build/libs/alfabot-telegram-0.0.1-SNAPSHOT.jar baw-bot.jar
ENTRYPOINT ["java", "-Dspring.profiles.active=prod", "-jar", "baw-bot.jar"]